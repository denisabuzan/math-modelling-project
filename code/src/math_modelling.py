import tensorflow as tf
import timeit
# from tensorflow.compat.v1.keras import backend as K
import json
from PIL import Image
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.externals._arff import xrange
from sklearn.model_selection import train_test_split
import tensorflow as tf
import tensorflow.keras as keras
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import MaxPooling2D, Flatten, Dense, Dropout, BatchNormalization, Conv2D
from tensorflow.keras.optimizers import RMSprop, Adam
from tensorflow.keras.callbacks import EarlyStopping, ModelCheckpoint, ReduceLROnPlateau, TensorBoard
from tensorflow.keras.models import load_model
from sklearn.metrics import confusion_matrix, classification_report, plot_confusion_matrix
import matplotlib.pyplot as plt
from sklearn.metrics import ConfusionMatrixDisplay
from tensorflow.python.keras.utils.vis_utils import plot_model

device_name = tf.test.gpu_device_name()
if device_name != '/device:GPU:0':
    print(
        '\n\nThis error most likely means that this notebook is not '
        'configured to use a GPU.  Change this in Notebook Settings via the '
        'command palette (cmd/ctrl-shift-P) or the Edit menu.\n\n')
    raise SystemError('GPU device not found')


def cpu():
    with tf.device('/cpu:0'):
        random_image_cpu = tf.random.normal((100, 100, 100, 3))
        net_cpu = tf.keras.layers.Conv2D(32, 7)(random_image_cpu)
        return tf.math.reduce_sum(net_cpu)


def gpu():
    with tf.device('/device:GPU:0'):
        random_image_gpu = tf.random.normal((100, 100, 100, 3))
        net_gpu = tf.keras.layers.Conv2D(32, 7)(random_image_gpu)
        return tf.math.reduce_sum(net_gpu)


# We run each op once to warm up; see: https://stackoverflow.com/a/45067900
cpu()
gpu()

# Run the op several times.
print('Time (s) to convolve 32x7x7x3 filter over random 100x100x100x3 images '
      '(batch x height x width x channel). Sum of ten runs.')
print('CPU (s):')
cpu_time = timeit.timeit('cpu()', number=10, setup="from _main_ import cpu")
print(cpu_time)
print('GPU (s):')
gpu_time = timeit.timeit('gpu()', number=10, setup="from _main_ import gpu")
print(gpu_time)
print('GPU speedup over CPU: {}x'.format(int(cpu_time / gpu_time)))



data_path1 = "C:\\Users\\napak\\Desktop\\"

data_path="/Users/denisa/Workspace/cassava-leaf-disease/data/cassava-leaf-disease-classification/"
plots_path = "../plots/"
IMG_SIZE = 255
size = (255, 255)
n_CLASS = 5
BATCH_SIZE = 16
EPOCHS = 15

"""------------READING AND PREPROCESS-------------"""
def read_files():
    """Reading the dataset from the csv file"""
    train_csv = pd.read_csv(data_path + 'train.csv')
    return train_csv

def preprocess_csv(data):
    """Adding a new column """
    with open(data_path + 'label_num_to_disease_map.json') as f:
        real_labels = json.load(f)
        real_labels = {int(k): v for k, v in real_labels.items()}

    data['class_name'] = data['label'].map(real_labels)
    return data


def augment_images(data):
    """Preprocessing the data and splitting it into training and validation"""
    train_gen = ImageDataGenerator(
        rescale=1. / 255,
        rotation_range=45,
        horizontal_flip=True,
        vertical_flip=True,
        fill_mode='nearest',
        validation_split=0.2
    )
    train_set = train_gen.flow_from_dataframe(
        data,
        directory=data_path + "train_images",
        seed=42,
        x_col='image_id',
        y_col='class_name',
        target_size=size,
        class_mode='categorical',
        shuffle=True,
        batch_size=BATCH_SIZE,
        subset='training'
    )

    val_set = train_gen.flow_from_dataframe(
        data,
        directory=data_path + "train_images",
        seed=42,
        x_col='image_id',
        y_col='class_name',
        target_size=size,
        class_mode='categorical',
        shuffle=False,
        batch_size=BATCH_SIZE,
        subset='validation'
    )

    return train_set, val_set


"""------------TRAINING THE MDOEL--------------"""

def create_model():
    """Creating the model architecture"""
    model = Sequential()
    inputShape = (IMG_SIZE, IMG_SIZE, 3)
    chanDim = -1

    model.add(Conv2D(32, (5, 5), padding="same", input_shape=inputShape, activation='relu',
                     kernel_initializer='lecun_uniform'))
    model.add(BatchNormalization(axis=chanDim))
    model.add(MaxPooling2D(pool_size=(2, 2)))

    model.add(Conv2D(64, (3, 3), padding="same", activation='relu', kernel_initializer='lecun_uniform'))
    model.add(BatchNormalization(axis=chanDim))
    model.add(Dropout(0.5))
    model.add(Conv2D(64, (3, 3), padding="same", activation='relu', kernel_initializer='lecun_uniform'))
    model.add(BatchNormalization(axis=chanDim))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.5))

    model.add(Conv2D(128, (3, 3), padding="same", activation='relu', kernel_initializer='lecun_uniform'))
    model.add(BatchNormalization(axis=chanDim))
    model.add(Dropout(0.5))
    model.add(Conv2D(128, (3, 3), padding="same", activation='relu', kernel_initializer='lecun_uniform'))
    model.add(BatchNormalization(axis=chanDim))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.5))

    model.add(Flatten())
    model.add(Dense(1024, activation='relu', kernel_initializer='lecun_uniform'))
    model.add(BatchNormalization())
    model.add(Dropout(0.5))

    model.add(Flatten(name="encoded"))
    model.add(Dense(1024, activation='relu', kernel_initializer='lecun_uniform'))
    model.add(BatchNormalization())
    model.add(Dropout(0.5))
    model.add(Dense(5, activation='softmax', kernel_initializer='lecun_uniform'))
    model.compile(
        optimizer=Adam(learning_rate=1e-4),
        loss=tf.keras.losses.CategoricalCrossentropy(
            name='categorical_crossentropy', from_logits=False
        ),
        metrics=['categorical_accuracy']
    )
    return model


def get_callbacks():
    """
    Creating the callbacks for the function.
    The best model is saved, with the lowest validation loss
    """
    checkpointer = ModelCheckpoint(
        "best_model.h5",
        save_best_only=True,
        monitor='val_loss',
        mode='min'
    )
    return [checkpointer]


def train_model(train_set, val_set):
    """Training the dataset"""
    model = create_model()
    callbacks = get_callbacks()
    history=model.fit(
        train_set,
        validation_data=val_set,
        epochs=EPOCHS,
        batch_size=BATCH_SIZE,
        callbacks=callbacks
    )
    plot_loss(history)
    plot_accuracy(history)
    plot_model(model, to_file='model.png')

    model.save('model.h5')
    return model


def visualize_data():
    train_csv = pd.read_csv(data_path + 'train.csv')
    train_csv['label'].value_counts().plot(kind='bar')
    plt.show()
    plt.savefig(plots_path+"plot_categories")
    print(train_csv['label'].value_counts())
    train_csv['label'] = train_csv['label'].astype('string')


"""------------Evalutaion--------------"""

def predict(model,val_set):
    print("a intrat in predict... ")
    """Generating the confusion matrix"""
    predictions = []
    images_test=[]
    size = (IMG_SIZE, IMG_SIZE)
    for image in val_set.filenames:
        img = Image.open(data_path + "train_images/" + image)
        img = img.resize(size)
        img = np.expand_dims(img, axis=0)
        images_test.append(img)
        predictions.extend(model.predict(img).argmax(axis=1))

    print(classification_report(val_set.labels, predictions))


def plot_loss(history):
    loss_train = history.history['loss']
    loss_val = history.history['val_loss']
    epochs = range(1, EPOCHS+1)
    plt.plot(epochs, loss_train, 'g', label='Training loss')
    plt.plot(epochs, loss_val, 'b', label='validation loss')
    plt.title('Training and Validation loss')
    plt.xlabel('Epochs')
    plt.ylabel('Loss')
    plt.legend()
    plt.savefig(plots_path+"plot_loss.png")
    plt.show()


def plot_accuracy(history):
    loss_train = history.history['categorical_accuracy']
    loss_val = history.history['val_categorical_accuracy']
    epochs = range(1, EPOCHS+1)
    plt.plot(epochs, loss_train, 'g', label='Training accuracy')
    plt.plot(epochs, loss_val, 'b', label='validation accuracy')
    plt.title('Training and Validation accuracy')
    plt.xlabel('Epochs')
    plt.ylabel('Accuracy')
    plt.savefig(plots_path+"plot_accuracy.png")
    plt.legend()
    plt.show()


if __name__ == "__main__":
    # visualize_data()
    data = read_files()
    data = preprocess_csv(data)
    train_set, val_set = augment_images(data)

    model = train_model(train_set, val_set)
    loaded_model = load_model('model.h5')
    predict(loaded_model, val_set)